# Build executables and packages

Some notes to build standalone packages for Linux, macOs and Windows

## Intsall pyinstaller:

See: https://pyinstaller.org/en/stable/installation.html

On Linux, macoOS, and Windows:

	pip3 install PyInstaller
	pip3 install --upgrade PyInstaller pyinstaller-hooks-contrib

It should be in your executable path (e.g., on Linux, maybe type PATH="$HOME/.local/bin:$PATH" if $HOME/.local/bin is not already in your path).

## Build an executable

Linux:

	pyinstaller -w --icon 'data/icons/pointsverts.ico' --add-data 'data/:data/' pointsverts.py

macOS:

	pyinstaller -w --icon 'data/icons/pointsverts.icns' --add-data 'data/:data/' pointsverts.py

Windows:

	pyinstaller -w --icon data\icons\pointsverts.ico --add-data data;data pointsverts.py


Then you can zip the *pointsverts* directory in the dist/ directory, e.g., on Linux/macOS:

		cd dist/
		zip -r pointsverts.zip pointsverts/

and end users just have to unzip pointsverts.zip, and execute pointsverts[.exe] in the created directory. However, we can do better, by building packages ...


## Building packages:

Now we will build packages from the above executables.

### A deb package for Linux (on Linux):

I used the following tutorial:
https://www.pythonguis.com/tutorials/packaging-pyqt5-applications-linux-pyinstaller/

Requirement:

	sudo apt install ruby
	gem install fpm --user-install
	PATH="$HOME/.local/share/gem/ruby/3.0.0/bin:$PATH"

Check (and edit if needed): deb/pointsverts.desktop deb/package.sh and deb/.fpm (in particular update the version in deb/.fpm) and run:

	cd deb/
	./package.sh
	fpm

This will build the debian package: pointsverts-x.x-x_foo.deb

End users can download the package and install it:

	sudo dpkg -i pointsverts-x.x-x_foo.deb

and run *pointsverts* as any other application.

To uninstall:

	sudo dpkg -r pointsverts


### A Disk Image for macOS (on macOS):

I used the following tutorial:
https://www.pythonguis.com/tutorials/packaging-pyqt5-applications-linux-pyinstaller/

Install create-dmg:

	brew install create-dmg

See https://github.com/create-dmg/create-dmg for alternative for installing create-dmg.

Then, edit/check the version in dmg/build-dmg.sh (VERSION=) and run

	cd dmg
	./build-dmg.sh

End users can then download pointsverts-installer-x.x-x.dmg and double-click on this image and drop the icon in the Application folder (see the wiki...)


### An Installer for Windows (on Windows):

I used the following tutorial:
https://www.pythonguis.com/tutorials/packaging-tkinter-applications-windows-pyinstaller/

Install InstallForge https://installforge.net/download/ (on Windows).

Start InstallForge and open the file InstallForge/pointsverts.ifp (in this repository). Then:

- In "General" check/update the version
- In Settings -> Files, remove all the files, and then add all the files in dist/pointsverts and then all the sub-directories (one by one)
- In "Build -> Build" update the setup file name .../pointsverts-x.x-x-setup.exe
- Then click on Build (top ~left of the window)
- You are done: the package is in InstallForge/

End users can download the package and install *pointsverts* by executing pointsverts-x.x-x-setup.exe. Then, they can run *pointsverts* via a desktop icon, the start menu or in the command prompt.


TODO: put the *pointsverts* path in the Windows execution path at install time (not sure this is a good idea). It can be done by hand. For the current command prompt:

	set PATH=c:\Program Files (x86)\LISN\pointsverts;%PATH%

Permanently, but work only for the next opened command prompts:
  
	setx PATH "c:\Program Files (x86)\LISN\pointsverts;%PATH%"

## Building an "official" release

First tag:

	git tag -a x.x -m "version x.x"
	git push origin x.x

In gitlab create a "New Release", use the tag above. We will add "Release assets" (aka the packages) later...

Build the packges as explain above and upload them somwhere.

Edit the Release to add links to these pacakges.

Edit the wiki to add the links to the new pacakges.
