# Moving objects detection

*pointsverts* has some code to detect moving objects in a video. This part is exploratory, and not used to help counting (at least for now).

## Enabling and using

To enable detection you should add the "--detect" option when starting *pointsverts* or press Ctrl-L when *pointsverts* is running.

I used a "standard" algorithm to detect moving objects (https://github.com/srijarkoroy/Moving-Object-Detection). Try it:

- press L to toggle the moving object detection mode (use -d option to enable at start or press Ctrl-L when *pointsverts* is running);
- press O to circulate to different object detection feedback: rectangular bounding boxes, contours, nothing;
- press I to circulate to different steps in the detection algorithm: normal (top left of the mosaic), mosaic, gray diff with the "background" (top right of the mosaic), threshold image where the contour algo is applied (bottom left of the mosaic), the "background" (bottom right of the mosaic);

to see that it does not work well for a dense crowd! 

A first step to improve this algorithm would be to improve the "background" image (maybe by hand). An object tracking algorithm should also be useful; there are more elaborated "contour" algorithms than the default OpenCv one. Then, we should investigate how an appropriate automatic objects detection method could help the hand counting method above.


## Counting people in an image of a crowd

There is lot of research on counting people in a crowd:

https://paperswithcode.com/task/crowd-counting

I tested P2PNet to create, given an image, an initial set of green dots for *pointsverts*:

https://github.com/tencentyouturesearch/crowdcounting-p2pnet

Note that the use of *P2PNet* is restricted to academic use (if I well understand)

### P2PNet howto

Follow the following step:

* Install:

You need a computer that support CUDA (NVIDIA GPU) and several python3 library with cuda support (pytorch, pytorchvision, etc.) or not (numpy, etc.): see the P2PNet Readme (I use the latest stable version of torch, torchvision, etc.). I use ubuntu 22 with the proprietary NVIDIA driver and CUDA libraries version 515 (PRIME configuration). I assume sutch a similar situation below.

In the *pointsverts* directory:

    git clone https://github.com/TencentYoutuResearch/CrowdCounting-P2PNet.git

Apply the p2pnet.patch patch:
    
    cd CrowdCounting-P2PNet
    git apply ../patches/p2pnet.patch
    mkdir logs/

* Use:

To detect people in an image path/image.jpg:

     __NV_PRIME_RENDER_OFFLOAD=1 CUDA_VISIBLE_DEVICES=0 python3 run_test.py --weight_path ./weights/SHTechA.pth --output_dir ./logs/  --image path/image.jpg --scale 0.35

if everything work well *P2PNet* build a file:

    path/image.dlcount

with the coordinates of the points where it detected a person. It also output images pred-[orig,scaled]-XXX.jpg in logs/ where XXX is the number of people detected in the image. If you have a "CUDA out of memory" error you should reduce the number of the --scale option (you can start with --scale 1.0 and reduce the scale until you do not get the memory error).

Then you can use *pointsverts*:

    python3 pointsverts.py -i path/image.jpg -ci path/image.dlcount

it will automatically load path/image.dlcount as count file (-ci option), and then you can edit the green dots (Warning: the ".count" file is used to save you work and any previous .count file will be erased).

## Other projects to look at:

https://github.com/theAIGuysCode/yolov4-deepsort
https://github.com/ultralytics/ultralytics
Could not make it to work with a Crowd

https://medium.com/aiguys/a-centroid-based-object-tracking-implementation-455021c2c997
https://github.com/srijarkoroy/Moving-Object-Detection
I have implemented this in pointverts ... nice and simple but does not work with a crowd

https://github.com/saimj7/People-Counting-in-Real-Time
elemental ... does not work with a crowd

https://github.com/Sentient07/HeadHunter
Tried ... unable to make it working

https://motchallenge.net/
some impressive results, lot of closed source project.
Click on the projects here: https://motchallenge.net/results/Head_Tracking_21/
For instance https://motchallenge.net/method/HT=31&chl=21 that seems to be based on: https://github.com/PaddlePaddle/PaddleDetection (open source)


## data set

https://github.com/HCIILAB/SCUT-HEAD-Dataset-Release

https://github.com/Asthestarsfalll/CrowdHuman