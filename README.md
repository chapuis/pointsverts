# pointsverts

*pointsverts* is a simple user interface (UI), written in python3 using PyQt5, OpenCV and numpy to count objects in a video or an image. It has been designed to count people (e.g., in a crowd), animals, or any other objects.

contact: olivier.chapuis@lisn.upsaclay.fr

## HowTo

If you just want to use *pointsverts* and you are not used to use python, you can use a *pointsverts* standalone package. See: https://gitlab.lisn.upsaclay.fr/chapuis/pointsverts/-/wikis/pointsverts.

### Install required software

As usual start with:

    git clone
    cd pointsverts

I assume that you have python3 and pip installed (see https://www.tutorialsteacher.com/python/install-python, or, on Ubuntu/Debian Linux system, install the python3 and python3-pip packages). 

Then run:

    pip install -r requirements.txt

On Ubuntu/Debian Linux system an alternative is to install the following packages: python3-pyqt5, python3-opencv, and python3-numpy.

### To run:

    python3 pointsverts.py path/video.mp4

Then, see the users documentation here:

https://gitlab.lisn.upsaclay.fr/chapuis/pointsverts/-/wikis/pointsverts

and in particular the usage senario and the Using section.

## Thanks

- Aldo Gonzalez-Lorenzo for "manifestants" https://gitlab.lis-lab.fr/aldo.gonzalez-lorenzo/manifestants
- Cécile Moulin for suggestions
- The "Algorithmique Participative" team for testing and encouraging the project
- Clair Mathieu to initiate the "Algorithmique Participative" project


## Moving objects detection


See Detection.md