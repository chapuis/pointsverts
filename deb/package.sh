#!/bin/sh
# Create folders.
[ -e package ] && rm -r package
mkdir -p package/opt
mkdir -p package/usr/bin
mkdir -p package/usr/share/applications
mkdir -p package/usr/share/icons/hicolor/scalable/apps

# Copy files (change icon names, add lines for non-scaled icons)
cp -r ../dist/pointsverts package/opt/pointsverts
cp ../data/icons/pointsverts.svg package/usr/share/icons/hicolor/scalable/apps/
cp pointsverts.desktop package/usr/share/applications/
cp pointsverts.sh package/usr/bin/pointsverts

# Change permissions
find package/opt/pointsverts -type f -exec chmod 644 -- {} +
find package/opt/pointsverts -type d -exec chmod 755 -- {} +
find package/usr/share -type f -exec chmod 644 -- {} +
chmod +x package/opt/pointsverts/pointsverts
chmod +x package/usr/bin/pointsverts