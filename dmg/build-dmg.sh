#!/bin/sh
VERSION=0.3-1
# Create a folder  to prepare our DMG in (if it doesn't already exist).
mkdir -p dist
# Empty the dmg folder.
rm -r dist/*
# Copy the app bundle to the dmg folder.
cp -r ../dist/pointsverts.app dist
# If the DMG already exists, delete it.
test -f "pointsverts-installer-$VERSION.dmg" && rm "pointsverts-installer-$VERSION.dmg"
create-dmg \
  --volname "pointsverst" \
  --volicon "../data/icons/pointsverts.icns" \
  --window-pos 200 120 \
  --window-size 600 300 \
  --icon-size 64 \
  --icon "pointsverts.app" 175 120 \
  --hide-extension "pointsverts.app" \
  --app-drop-link 425 120 \
  "pointsverts-installer-$VERSION.dmg" \
  "dist/"