import os
import sys
import argparse
import math

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QInputDialog, QFileDialog

import numpy as np
import cv2

VERSION=1.0

os.environ["QT_QPA_PLATFORM_PLUGIN_PATH"] = QtCore.QLibraryInfo.location(
    QtCore.QLibraryInfo.PluginsPath
)

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)

def qcolor_to_color(qcol):
    return (qcol.blue(), qcol.green(), qcol.red())

def color_to_qcolor(col):
    return QtGui.QColor(col[2], col[1], col[0])

def rotate(p, angle, c):
    (x, y) = (p[0] - c[0], p[1] - c[1])
    (xr, yr) = (x*math.cos(angle) - y*math.sin(angle), x*math.sin(angle) + y*math.cos(angle))
    (xr, yr) = (xr + c[0], yr + c[1])
    return (xr, yr)

def angle_between_lines(p1, p2, q1, q2):
    (dx1, dy1) = (p2[0] - p1[0], p2[1] - p1[1])
    (dx2, dy2) = (q2[0] - q1[0], q2[1] - q1[1])
    if dx1 == 0: # vertical
        if dx2 == 0:
            return 0
        else:
            return 90 - math.degrees(math.atan(abs(dy2/dx2))) ## FIXME !!
    elif dx2 == 0:
        return 90 - math.degrees(math.atan(abs(dy1/dx1))) ## FIXME !!
    (m1, m2) = (dy1/dx1, dy2/dx2)
    if m1*m2 == -1:
        return(90)
    ret = math.atan((m1-m2)/(1+m1*m2))
    return math.degrees(ret)

class TQtPoint():
    def __init__(self, f, p):
        self.f = f  # frame pos
        self.p = p  # QPoint

class CatTQtPoint():
    def __init__(self, c, p):
        self.c = c  # category
        self.tp = p #TQtPoint

class Undo():
    def __init__(self, what, action, ctpoints):
        self.what = what # humain descrition
        self.action = action # add or remove
        self.ctpoints = ctpoints # points to act on

class Line():
    def __init__(self, p1, p2, color):
        self.p1 = p1
        self.p2 = p2
        self.color = color

    def to_qcolor(self):
        return QtGui.QColor(self.color[2], self.color[1], self.color[0])

class LinesEditor(QtWidgets.QDialog):
    def __init__(self, parent, lines):
        super().__init__(parent)
        self.setWindowTitle("Lines Editor")
        self.setModal(False)
        self.win = parent
        self.save_lines = lines
        self.new_lines = []
        if lines is not None and len(lines) > 0:
            # self.new_lines = lines.copy() # does not work because of the color
            for l in lines:
                ll = Line(l.p1, l.p2, (l.color[0], l.color[1], l.color[2]))
                self.new_lines.append(ll)
        self.win.viewer.set_lines(self.new_lines)

        self.main_qcolor = QtGui.QColor(255, 0, 0)
        self.zone_qcolor = QtGui.QColor(0, 0, 255)
        self.num_zone_lines = 4
        self.zone_angle = 90

        if len(self.new_lines) > 0:
            self.main_qcolor = self.new_lines[0].to_qcolor()

        if len(self.new_lines) > 1:
            self.num_zone_lines = len(self.new_lines) - 1
            self.zone_qcolor = self.new_lines[1].to_qcolor()
            (p1, p2) = (self.new_lines[0].p1, self.new_lines[0].p2)
            (q1, q2) = (self.new_lines[1].p1, self.new_lines[1].p2)
            self.zone_angle = abs(int(angle_between_lines(p1, p2, q1, q2)))

        QBtn = QtWidgets.QDialogButtonBox.Save | QtWidgets.QDialogButtonBox.Cancel
        buttonBox = QtWidgets.QDialogButtonBox(QBtn)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        vblayout = QtWidgets.QVBoxLayout()
        message = QtWidgets.QLabel("Hope that you read the doc :)")
        vblayout.addWidget(message)

        hblayout = QtWidgets.QHBoxLayout()
        hblayout.addWidget(QtWidgets.QLabel("Main Line:"))
        main_line = QtWidgets.QPushButton(self)
        main_line.setText("Choose two points")
        main_line.clicked.connect(self.choose_two_points)
        self.main_line_label = QtWidgets.QLabel("(xxxx,xxxx) (yyyy,yyyy)")
        self.main_colorchoice = QtWidgets.QPushButton(self)
        # self.main_colorchoice.setIcon(ColorIcon(20, self.main_qcolor, "rect"))
        self.main_colorchoice.setText("Choose a color")
        self.main_colorchoice.clicked.connect(self.choose_main_color)
        if len(self.new_lines) > 0:
            self.main_line_label.setText(str(self.new_lines[0].p1)+" "+str(self.new_lines[0].p2))
        self.main_colorchoice.setIcon(ColorIcon(20, self.main_qcolor, "rect"))
        hblayout.addWidget(main_line)
        hblayout.addWidget(self.main_line_label)
        hblayout.addWidget(self.main_colorchoice)
        vblayout.addLayout(hblayout)
        #
        hzlayout = QtWidgets.QHBoxLayout()
        hzlayout.addWidget(QtWidgets.QLabel("Zone Lines:"))
        #
        hz2layout = QtWidgets.QHBoxLayout()
        #
        self.zone_lines = QtWidgets.QLabel(self)
        self.zone_lines.setText("# of lines:")
        hz2layout.addWidget(self.zone_lines)
        #
        self.zone_lines_edit = QtWidgets.QSpinBox(self)
        self.zone_lines_edit.setMaximum(100)
        self.zone_lines_edit.setMinimum(0)
        self.zone_lines_edit.setValue(self.num_zone_lines)
        self.zone_lines_edit.valueChanged.connect(self.zone_lines_changed)
        #self.zone_lines.clicked.connect(self.start_define_zone_lines)
        hz2layout.addWidget(self.zone_lines_edit)
        #
        self.zone_angle_label = QtWidgets.QLabel(self)
        self.zone_angle_label.setText("angle with main:")
        hz2layout.addWidget(self.zone_angle_label)
        #
        self.zone_angle_edit = QtWidgets.QSpinBox(self)
        self.zone_angle_edit.setMaximum(179)
        self.zone_angle_edit.setMinimum(1)
        self.zone_angle_edit.setValue(self.zone_angle)
        self.zone_angle_edit.valueChanged.connect(self.zone_angle_changed)
        #self.zone_angle_edit.clicked.connect(self.start_define_zone_angle_edit)
        hz2layout.addWidget(self.zone_angle_edit)
        #
        self.zone_colorchoice = QtWidgets.QPushButton(self)
        self.zone_colorchoice.setIcon(ColorIcon(20, self.zone_qcolor, "rect"))
        self.zone_colorchoice.setText("Choose a color")
        self.zone_colorchoice.clicked.connect(self.choose_zone_color)
        hz2layout.addWidget(self.zone_colorchoice)
        #
        hz3layout = QtWidgets.QHBoxLayout()
        save_template = QtWidgets.QPushButton(self)
        save_template.setText("Save as count template")
        save_template.clicked.connect(self.save_as_template)
        hz3layout.addWidget(save_template)
        hz3layout.addWidget(buttonBox)
        #
        vblayout.addLayout(hzlayout)
        vblayout.addLayout(hz2layout)
        vblayout.addLayout(hz3layout)
        #vblayout.addWidget(buttonBox)
        self.setLayout(vblayout)

    def save_as_template(self):
        fname , _ = QFileDialog.getSaveFileName(self, 'Save File', "template.count")
        if fname:
            self.win.write_template(fname, self.new_lines)

    def choose_main_color(self):
        dialog = QtWidgets.QColorDialog(self)
        dialog.setCurrentColor(self.main_qcolor)
        if dialog.exec_():
            self.main_qcolor = dialog.selectedColor()
            self.main_colorchoice.setIcon(ColorIcon(20, self.main_qcolor, "rect"))
        if len(self.new_lines) > 0:
            self.new_lines[0].color = qcolor_to_color(self.main_qcolor)
            self.win.viewer.redraw_points()

    def choose_zone_color(self):
        dialog = QtWidgets.QColorDialog(self)
        dialog.setCurrentColor(self.zone_qcolor)
        if dialog.exec_():
            self.zone_qcolor = dialog.selectedColor()
            self.zone_colorchoice.setIcon(ColorIcon(20, self.zone_qcolor, "rect"))
        for i in range(len(self.new_lines)):
            if i >= 1:
                self.new_lines[i].color = qcolor_to_color(self.zone_qcolor)
        self.win.viewer.redraw_points()

    def choose_two_points(self):
        self.win.viewer.choose_two_points(self)

    def zone_lines_changed(self, v):
        self.num_zone_lines = v
        self.compute_zone_lines()

    def zone_angle_changed(self, v):
        self.zone_angle = v
        self.compute_zone_lines()

    def set_points(self, p1, p2):
        #print("get the two points", p1, p2)
        self.main_line_label.setText(str(p1)+" "+str(p2))
        col = qcolor_to_color(self.main_qcolor)
        # FIXME p1 == p2
        if p1[0] < p2[0]:
            l = Line(p1, p2, col)
        else:
            l = Line(p2, p1, col)
        if len(self.new_lines) > 0:
            self.new_lines[0] = l
        else:
            self.new_lines.append(l)
        self.compute_zone_lines()
        self.win.viewer.redraw_points()
        #self.zone_lines.setDisabled(False)
        #self.zone_lines.setText("Define the zone lines")

    def compute_zone_lines(self):
        if len(self.new_lines) < 1:
            return
        l = self.new_lines[0]
        self.new_lines.clear()
        self.new_lines.append(l)
        dist = ((l.p1[0] - l.p2[0])**2 + (l.p1[1] - l.p2[1])**2)**0.5
        if dist == 0 or self.num_zone_lines == 0:
            self.win.viewer.redraw_points()
            return
        # line equation y = mx + b
        (dx, dy) = (l.p2[0] - l.p1[0], l.p2[1] - l.p1[1])
        m = 0
        if dx != 0:
            m = dy/dx
        b =  l.p1[1] - m*l.p1[0]
        incx = (l.p2[0] - l.p1[0]) / (self.num_zone_lines + 1)
        incy = abs(l.p2[1] - l.p1[1]) / (self.num_zone_lines + 1)
        x = startx = l.p1[0]
        starty = min(l.p2[1], l.p1[1])
        dist = dist/2
        for i in range(self.num_zone_lines):
            x = x + incx
            y = m*x + b
            (q1, q2) = ((0,0), (0,0))
            if m == 0:
                if dx == 0: # vertical
                    (q1, q2) = ((x-dist, starty + incy*(i+1)), (x+dist, starty + incy*(i+1)))
                else: # y = b
                    (q1, q2) = ((x, b-dist), (x, b+dist))

            else:
                # line otho to main line: Y = om.X + ob // om.X - Y + ob = 0
                om = -1/m
                ob = y - om*x
                (q1, q2) = ((x-dist, om*(x-dist) + ob), (x+dist, om*(x+dist) + ob))
            q1 = rotate(q1, math.radians(self.zone_angle -90), (x,y))
            q2 = rotate(q2, math.radians(self.zone_angle -90), (x,y))
            (q1, q2) = ((int(q1[0]), int(q1[1])), (int(q2[0]), int(q2[1])))
            self.new_lines.append(Line(q1, q2, qcolor_to_color(self.zone_qcolor)))
        self.win.viewer.redraw_points()

    def accept(self):
        self.win.viewer.choose_two_points_cancel()
        # nothing to do... but saving
        self.win.reset_count_file(self.win.viewer.cats, self.win.viewer.lines)
        super(LinesEditor, self).accept()

    def reject(self):
        self.win.viewer.choose_two_points_cancel()
        self.win.viewer.set_lines(self.save_lines)
        self.win.viewer.redraw_points()
        super(LinesEditor, self).reject()

class CategoryDialog(QtWidgets.QDialog):
    def __init__(self, parent, what, color=QtGui.QColor(75, 255, 255), name=""):
        super().__init__(parent)
        self.setWindowTitle(what + " Category")

        self.name = name
        self.color = color
        QBtn = QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel
        buttonBox = QtWidgets.QDialogButtonBox(QBtn)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        vblayout = QtWidgets.QVBoxLayout()
        message = QtWidgets.QLabel(what + " Category")
        vblayout.addWidget(message)
        
        hblayout = QtWidgets.QHBoxLayout()
        hblayout.addWidget(QtWidgets.QLabel("Name:"))
        editname = QtWidgets.QLineEdit(self)
        editname.setText(self.name)
        editname.textChanged.connect(self.name_text)
        hblayout.addWidget(editname)

        self.colorchoice = QtWidgets.QPushButton(self)
        self.colorchoice.setIcon(ColorIcon(20, self.color))
        self.colorchoice.setText("Choose a color")
        self.colorchoice.clicked.connect(self.choose_color)
        hblayout.addWidget(self.colorchoice)

        vblayout.addLayout(hblayout)
        vblayout.addWidget(buttonBox)
        self.setLayout(vblayout)

    def choose_color(self):
        dialog = QtWidgets.QColorDialog(self)
        dialog.setCurrentColor(self.color)
        if dialog.exec_():
            self.color = dialog.selectedColor()
            self.colorchoice.setIcon(ColorIcon(20, self.color))

    def name_text(self, s):
        self.name = s

    def get_result(self):
        return(self.color, self.name)

class ColorIcon(QtGui.QIcon):
    def __init__(self, size, color, shape="circle"):
        super().__init__()
        pix = QtGui.QPixmap(size, size)
        lineWidth = 2
        ray = int((size-2)/2)
        x = y = +1
        pix.fill(QtCore.Qt.transparent)
        painter = QtGui.QPainter(pix)
        pen = QtGui.QPen(QtCore.Qt.black)
        pen.setWidth(2)
        brush = QtGui.QBrush(color, QtCore.Qt.SolidPattern)
        painter.setPen(pen)
        if shape == "rect":
            painter.drawRect(x,y+int(ray/1.5),2*ray,ray)
        else:
            painter.drawEllipse(x,y,2*ray,2*ray)
        painter.setBrush(brush)
        if shape == "rect":
            painter.drawRect(x,y+int(ray/1.5),2*ray,ray)
        else:
            painter.drawEllipse(x,y,2*ray,2*ray)
        painter.end()

        self.addPixmap(pix)

# improved slider, thanks to https://stackoverflow.com/users/6622587/eyllanesc
class PVSlider(QtWidgets.QSlider):

    def mousePressEvent(self, e):
        if e.button() == QtCore.Qt.LeftButton:
            x = e.pos().x()
            #value = (self.maximum() - self.minimum()) * x / self.width() + self.minimum()
            value = self.pixelPosToRangeValue(e.pos())
            self.setValue(int(value))
        super(PVSlider, self).mousePressEvent(e)

    def pixelPosToRangeValue(self, pos):
        opt = QtWidgets.QStyleOptionSlider()
        self.initStyleOption(opt)
        gr = self.style().subControlRect(QtWidgets.QStyle.CC_Slider, opt, QtWidgets.QStyle.SC_SliderGroove, self)
        sr = self.style().subControlRect(QtWidgets.QStyle.CC_Slider, opt, QtWidgets.QStyle.SC_SliderHandle, self)

        if self.orientation() == QtCore.Qt.Horizontal:
            sliderLength = sr.width()
            sliderMin = gr.x()
            sliderMax = gr.right() - sliderLength + 1
        else:
            sliderLength = sr.height()
            sliderMin = gr.y()
            sliderMax = gr.bottom() - sliderLength + 1;
        pr = pos - sr.center() + sr.topLeft()
        p = pr.x() if self.orientation() == QtCore.Qt.Horizontal else pr.y()
        return QtWidgets.QStyle.sliderValueFromPosition(self.minimum(), self.maximum(), p - sliderMin, sliderMax - sliderMin, opt.upsideDown)

# add checked items and fixe the focus ...
class PVComboBox(QtWidgets.QComboBox):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent

    def addCheckableItem(self, item):
        super(PVComboBox, self).addItem(item)
        item = self.model().item(self.count()-1,0)
        item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        item.setCheckState(QtCore.Qt.Unchecked)

    def itemChecked(self, index):
        item = self.model().item(index,0)
        return item.checkState() == QtCore.Qt.Checked

    def setCheckState(self, index, doset):
        item = self.model().item(index,0)
        if doset:
            item.setCheckState(QtCore.Qt.Checked)
        else:
            item.setCheckState(QtCore.Qt.Unchecked)

    # to fixe the focus ... horrible
    # maybe use an event filter in Window, see https://doc.qt.io/qtforpython/overviews/eventsandfilters.html
    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Z:
            self.parent.next(event)
        elif event.key() == QtCore.Qt.Key_A:
            self.parent.prev(event)
        elif event.key() == QtCore.Qt.Key_Q:
            self.parent.prev_long(event)

        super(PVComboBox, self).keyPressEvent(event)

# video and image handler
class Video():
    def __init__(self, parent, args):
        self.parent = parent
        self.cap = None
        self.nbr_frames = 0
        self.width = 0
        self.height = 0
        self.fps = 1

        # current frame position
        self.frame_pos = -1

        self.tpoint_per_nframe = args["persistence"]
        self.show_all_points = False
        self.draw_width = 6
        self.last_frame = None
        self.last_frame_notpoints = None
        self.one_frame = None
        self.show_all_points = False
        self.show_all_cats = False
        self.cat_colors = []

        # for detection algo
        self.detect = args["detect"]
        self.nbr_frames_for_avg = 30
        self.gray_frame_avg = None
        self.gray_frame_avg_copy = None
        self.contours = []
        self.shows_list = ["video", "mosaic", "gray", "threshold", "background"]
        self.shows_index = 0
        self.drawc_list = ["rect", "contour", "none"]
        self.drawc_index = 0


    def set_pesistance(self, per):
        self.tpoint_per_nframe = per
    
    def update_drawing_size(self, w):
       self.draw_width = w

    def set_cat_colors(self, colors):
        self.cat_colors = colors

    def toggle_show_all_points(self):
        if self.show_all_points:
            self.show_all_points = False
        else:
            self.show_all_points = True
    
    def toggle_show_all_cats(self):
        if self.show_all_cats:
            self.show_all_cats = False
        else:
            self.show_all_cats = True

    def toggle_detect(self):
        if self.detect:
            self.detect = False
            self.contours = []
        else:
            if self.gray_frame_avg is None and self.cap is not None:
                self.compute_avg_frame()
            self.detect = True

    def enable_detect(self):
        if self.detect:
            return
        self.toggle_detect()

    def next_draw_contour(self):
        self.drawc_index += 1
        if self.drawc_index > len(self.drawc_list)-1:
            self.drawc_index = 0

    def next_shows_image(self):
        self.shows_index += 1
        if self.shows_index > len(self.shows_list)-1:
            self.shows_index = 0

    def is_mosaic(self):
        return self.shows_list[self.shows_index] == "mosaic"
    
    def compute_avg_frame(self):
        progress = QtWidgets.QProgressDialog("collecting frame ...", "Cancel", 0, int(self.nbr_frames), self.parent)
        progress.setWindowTitle("computing...")
        progress.setWindowModality(QtCore.Qt.WindowModal)
        progress.show()
        QtWidgets.QApplication.processEvents()
        #print("computing frame avg ...", self.nbr_frames, self.nbr_frames_for_avg, end ='', flush=True)
        if self.nbr_frames < self.nbr_frames_for_avg:
            self.gray_frame_avg = None
            progress.setValue(int(self.nbr_frames))
            return
        step_frame_get = int(self.nbr_frames/self.nbr_frames_for_avg)
        frames = []
        i = 0
        while i < self.nbr_frames:
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, i)
            ret, frame = self.cap.read()
            if ret:
                frames.append(frame)
            progress.setValue(i)
            QtWidgets.QApplication.processEvents()
            if progress.wasCanceled():
                break
            i = i + step_frame_get
        
        progress.setLabelText("computing median frame...")
        QtWidgets.QApplication.processEvents()
        frame_avg = np.median(frames, axis = 0).astype(dtype = np.uint8)
        # frame_avg = np.average(frames, axis = 0).astype(dtype = np.uint8)
        self.gray_frame_avg = cv2.cvtColor(frame_avg, cv2.COLOR_BGR2GRAY)
        self.gray_frame_avg_copy = self.gray_frame_avg.copy()
        self.cap.set(cv2.CAP_PROP_POS_FRAMES, self.frame_pos) 
        progress.setValue(int(self.nbr_frames))

    def load_video(self, filename):
        print("opening ", filename)
        self.nbr_frames = 0
        what = "video"
        if cv2.haveImageReader(filename):
            print(filename, "is an image")
            what = "image"
            self.one_frame = cv2.imread(filename)
            dim = self.one_frame.shape
            self.cap = None
            self.nbr_frames = 1
            self.width = dim[1]
            self.height = dim[0]
            self.fps = 1
        else:
            self.cap = cv2.VideoCapture(filename)
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
            self.nbr_frames = self.cap.get(cv2.CAP_PROP_FRAME_COUNT)
            self.width = self.cap.get(cv2.CAP_PROP_FRAME_WIDTH)
            self.height = self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
            self.fps = self.cap.get(cv2.CAP_PROP_FPS)
        if self.fps == 0:
            self.fps = 1
        print("A %s, size %d x %d, fps: %d, #frames: %d, duration: %d s" %
            (what, self.width, self.height, self.fps, self.nbr_frames, 
            self.nbr_frames/self.fps))

        if self.nbr_frames == 0:
            return(False)
        
        # average backround
        self.gray_frame_avg = None
        self.gray_frame_avg_copy = None
        if self.detect:
            self.compute_avg_frame()
        self.frame_pos = -1
        self.contours = []
        return(True)

    def set_frame_pos(self, f):
        if f < 0:
            f = 0
        if self.cap is not None:
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, f)
        self.frame_pos = f - 1

    def frame_to_pixmap(self, frame):
        rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_frame.shape
        bytesPerLine = ch * w
        convertToQtFormat = QtGui.QImage(rgb_frame.data, w, h, bytesPerLine, QtGui.QImage.Format_RGB888)
        return(QtGui.QPixmap.fromImage(convertToQtFormat))

    def draw_contour_on(self, frame):
        #print("contours:", len(self.contours))
        if len(self.contours) > 0:
            if self.drawc_list[self.drawc_index] == "contour":
                frame = cv2.drawContours(frame, self.contours, -1, (255,255,75), 2)
            elif self.drawc_list[self.drawc_index] == "rect":
                for i in self.contours:
                    x, y, width, height = cv2.boundingRect(i)
                    cv2.circle(frame, (int(x+width/2),int(y+height/2)), 2, (255,255,75), -1)
                    cv2.rectangle(frame, (x,y), (x + width, y + height), (255,255,75), 2)
        return frame

    def do_show_point(self, tp):
        return self.show_all_points or (self.frame_pos >= tp.f and  self.frame_pos <= tp.f + self.tpoint_per_nframe)

    def draw_tpoints_on(self, frame, tpoints, color):
        lineWidth = max(1,int(self.draw_width/4))
        ray = self.draw_width
        scale = 1
        r = ray
        if self.is_mosaic():
            scale = 0.5
            r = int(scale*ray)
        overlay = frame.copy()
        #overlay = frame
        for tp in tpoints:
            if  self.do_show_point(tp):
                x = int(scale*tp.p.x())
                y = int(scale*tp.p.y())
                cv2.circle(overlay, (x,y), r, color, -1)
                cv2.circle(overlay, (x,y), r, (0,0,0), lineWidth)
        alpha = 0.6
        image_new = cv2.addWeighted(overlay, alpha, frame, 1 - alpha, 0)
        #image_new = overlay
        return image_new

    def draw_lines_on(self, frame, lines):
        thickness = max(2,int(self.draw_width/4))
        scale = 1.0
        if self.is_mosaic():
            scale = 0.5
        thickness = int(thickness*scale)
        for i in range(len(lines)-1, -1, -1):
            l = lines[i]
            frame = cv2.line(frame, l.p1, l.p2, l.color, thickness)
        return frame


    def get_next_frame(self, c, cats, lines, ret_frame=False):
        frame = None
        if self.cap is not None:
            # video
            ret, frame = self.cap.read()
            self.frame_pos += 1
            if frame is None:
                self.frame_pos -= 1
                #print("end of video", self.frame_pos)
                return None
        elif self.one_frame is not None:
            # image
            self.frame_pos += 1
            if self.frame_pos == 0:
                frame = self.one_frame.copy()
            else:
                self.frame_pos -= 1
                return None
        else:
                return None

        gray_frame = blur_frame = threshold_frame = None 
        if self.detect: # and self.gray_frame_avg is not None:
            gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            avg_frame = None
            if self.frame_pos % 1 == 0:
                if self.gray_frame_avg is not None:
                    gray_frame = cv2.absdiff(gray_frame, self.gray_frame_avg)
                blur_frame = cv2.GaussianBlur(gray_frame, (11,11), 0)
                ret, threshold_frame = cv2.threshold(blur_frame, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
                (self.contours, _ ) = cv2.findContours(threshold_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        dframe = frame
        if gray_frame is not None:
            if self.shows_list[self.shows_index] == "gray":
                dframe = cv2.cvtColor(gray_frame, cv2.COLOR_GRAY2BGR)
            elif self.shows_list[self.shows_index] == "threshold":
                dframe = cv2.cvtColor(threshold_frame, cv2.COLOR_GRAY2BGR)
            elif self.shows_list[self.shows_index] == "background" and self.gray_frame_avg_copy is not None:
                dframe = cv2.cvtColor(self.gray_frame_avg, cv2.COLOR_GRAY2BGR)
            elif self.shows_list[self.shows_index] == "mosaic":
                dim = (int(self.width/2), int(self.height/2))
                frames = []
                frames.append(cv2.resize(self.draw_contour_on(frame), dim))
                frames.append(cv2.resize(self.draw_contour_on(cv2.cvtColor(blur_frame, cv2.COLOR_GRAY2BGR)), dim))
                frames.append(cv2.resize(self.draw_contour_on(cv2.cvtColor(threshold_frame, cv2.COLOR_GRAY2BGR)), dim))
                if self.gray_frame_avg is not None:
                    frames.append(cv2.cvtColor(cv2.resize(self.gray_frame_avg, dim), cv2.COLOR_GRAY2BGR))
                else:
                    frames.append(cv2.resize(frame, dim))
                n_rows=2
                n_cols=2
                rows_list = [frames[x:x+n_cols] for x in range(0, len(frames), n_cols)]
                rows = [np.hstack(row) for row in rows_list]
                dframe = np.vstack(rows)
        
        if not self.shows_list[self.shows_index] == "mosaic":
            dframe = self.draw_contour_on(dframe)

        self.last_frame_notpoints = dframe.copy()
        dframe = self.draw_lines_on(dframe, lines)
        if self.show_all_cats:
            for ac in range(len(cats)):
                dframe = self.draw_tpoints_on(dframe, cats[ac], self.cat_colors[ac])
        else:
            dframe = self.draw_tpoints_on(dframe, cats[c], self.cat_colors[c])

        self.last_frame = dframe
        if ret_frame:
            return(dframe)
        return(self.frame_to_pixmap(dframe))

    def get_current_frame(self, c, cats, lines, ret_frame=False):
        self.set_frame_pos(self.frame_pos - 1)  
        return(self.get_next_frame(c, cats, lines, ret_frame))

    def add_points(self, c, tpoints):
        self.last_frame = self.draw_tpoints_on(self.last_frame, tpoints, self.cat_colors[c])
        pix = self.frame_to_pixmap(self.last_frame)
        return pix

    def redraw_points(self, c, cats, lines):
        if self.last_frame_notpoints is None:
            return
        self.last_frame = self.last_frame_notpoints.copy()
        self.last_frame = self.draw_lines_on(self.last_frame, lines)
        if self.show_all_cats:
            for ac in range(len(cats)):
                self.last_frame = self.draw_tpoints_on(self.last_frame, cats[ac], self.cat_colors[ac])
        else:
            self.last_frame = self.draw_tpoints_on(self.last_frame, cats[c], self.cat_colors[c])
        pix = self.frame_to_pixmap(self.last_frame)
        return pix

# 
class ImageViewer(QtWidgets.QGraphicsView):
    point_added = QtCore.pyqtSignal(int, TQtPoint)
    update_count = QtCore.pyqtSignal(int)
    reset_count_file = QtCore.pyqtSignal(list, list)
    scene_changed = QtCore.pyqtSignal()

    def __init__(self, parent, video):
        super(ImageViewer, self).__init__(parent)
        self.video = video
        self.empty = True
        self.press = None

        # the data
        self.counts = []
        self.cats = []
        self.cc = 0
        self.lines = []

        # interaction helper
        self.press_modifiers = None
        self.undos = []
        self.redos = []
        # lines
        self.lines_editor = None
        self.lines_editor_p1 = None
        self.lines_editor_p2 = None

        self.scene = QtWidgets.QGraphicsScene(self)
        self.image = QtWidgets.QGraphicsPixmapItem()
        self.pixmap = QtGui.QPixmap()
        self.scene.addItem(self.image)
        self.setScene(self.scene)

        self.scene.changed.connect(self.changed)

        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setBackgroundBrush(QtGui.QBrush(QtGui.QColor(30, 30, 30)))
        self.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)

    def clean_data(self):
        self.counts = []
        self.cats = []
        self.cc = 0
        self.lines = []
        self.undos = []
        self.redos = []

    def add_category(self, c):
        if len(self.cats) == c:
            #print("viewer add category", c)
            self.cats.append([])
            self.counts.append(0)
        else:
            print("viewer add category should not happen", c, len(self.cats))

    def remove_category(self, c):
        if c > 1: # shoul be true anyway
            #print("remove category", c)
            if c < len(self.cats):
                del self.cats[c]
                del self.counts[c]

    def change_category(self, c):
        if c < 0 or c >= len(self.cats):
            print("change_category viewer: should not happen", c, len(self.cats))
            return
        self.cc = c
        self.update_count.emit(self.counts[self.cc])
        self.redraw_points()

    def fit_in_view(self, scale=True):
        rect = QtCore.QRectF(self.image.pixmap().rect())
        if not rect.isNull():
            self.setSceneRect(rect)
            if self.has_image():
                unity = self.transform().mapRect(QtCore.QRectF(0, 0, 1, 1))
                self.scale(1 / unity.width(), 1 / unity.height())
                viewrect = self.viewport().rect()
                scenerect = self.transform().mapRect(rect)
                factor = min(viewrect.width() / scenerect.width(),
                             viewrect.height() / scenerect.height())
                self.scale(factor, factor)

    def has_image(self):
        return not self.empty

    def set_image(self, pixmap=None):
        if pixmap and not pixmap.isNull():
            self.empty = False
            self.pixmap = pixmap
            self.image.setPixmap(pixmap)
        else:
            self.empty = True
            self.image.setPixmap(QtGui.QPixmap())

    def changed(self, region):
        # print("changed !")
        self.scene_changed.emit()

    def set_lines(self, lines):
        self.lines = lines

    def choose_two_points(self, le):
        print("viewer: choose_two_points")
        self.lines_editor = le
        self.lines_editor_p1 = None
        self.lines_editor_p2 = None

    def choose_two_points_cancel(self):
        self.lines_editor = None

    def add_points(self, tpoints):
        ctps = []
        for tp in tpoints:
            self.counts[self.cc] += 1
            self.cats[self.cc].append(tp)
            ctp = CatTQtPoint(self.cc, tp)
            ctps.append(ctp)
        if len(ctps) > 1:
            self.undos.append(Undo("add points", "add", ctps))
        else:
            self.undos.append(Undo("add point", "add", ctps))
        pix = self.video.add_points(self.cc, tpoints)
        self.image.setPixmap(pix)
        self.update_count.emit(self.counts[self.cc])

    def reset_points_data(self, c, tpoints):
        self.counts[c] = 0
        self.cats[c].clear()
        self.undos = []
        self.redos = []
        for tp in tpoints:
            self.counts[c] += 1
            self.cats[c].append(tp)
        if c == self.cc:
            self.update_count.emit(self.counts[self.cc])

    def add_one_point(self, tpoint):
        ps = []
        ps.append(tpoint)
        self.add_points(ps)
        self.point_added.emit(self.cc, tpoint)
    
    def redraw_points(self):
        if self.empty:
            return
        pix = self.video.redraw_points(self.cc, self.cats, self.lines)
        self.image.setPixmap(pix)

    def remove_point(self, c, index):
        if c < 0 or c > len(self.cats) or index < 0 or index >= len(self.cats[c]):
            return()
        self.undos.append(Undo("remove point", "remove", [CatTQtPoint(c, self.cats[c][index])]))
        del self.cats[c][index]
        self.counts[c] -= 1
        self.redraw_points()
        self.reset_count_file.emit(self.cats, self.lines)
        self.update_count.emit(self.counts[self.cc]) # yes self.cc

    def remove_points(self, rect):
        has_removed = False
        ctps = []
        for c in range(len(self.cats)-1, -1, -1):
            if not self.video.show_all_cats and c != self.cc:
                next
            for i in range(len(self.cats[c])-1, -1, -1):
                tp = self.cats[c][i]
                if self.video.do_show_point(tp):
                    if tp.p.x() >= rect.x() and tp.p.x() <= rect.x() + rect.width() and tp.p.y() >= rect.y() and tp.p.y() <= rect.y() + rect.height():
                        ctps.append(CatTQtPoint(c, self.cats[c][i]))
                        del self.cats[c][i]
                        self.counts[c] -= 1
                        has_removed = True
        if has_removed:
            self.undos.append(Undo("remove points", "remove", ctps))
            self.redraw_points()
            self.reset_count_file.emit(self.cats, self.lines)
            self.update_count.emit(self.counts[self.cc]) # yes self.cc

    def undoredo(self, u, is_undo):
        #print("undo ", u.what, is_undo)
        if (u.action == "remove" and is_undo) or (u.action == "add" and not is_undo):
            # add ...
            for ctp in u.ctpoints:
                if ctp.c < len(self.cats):
                    self.counts[ctp.c] += 1
                    self.cats[ctp.c].append(ctp.tp)
        elif (u.action == "add" and is_undo) or (u.action == "remove" and not is_undo):
            # remove ...
            for ctp in u.ctpoints:
                if ctp.c < len(self.cats):
                    for i in range(len(self.cats[ctp.c])):
                        tp = self.cats[ctp.c][i]
                        if (tp.f == ctp.tp.f and tp.p.x() == ctp.tp.p.x() and  tp.p.y() == ctp.tp.p.y()):
                            del self.cats[ctp.c][i]
                            self.counts[ctp.c] -= 1
                            break
        self.redraw_points()
        self.reset_count_file.emit(self.cats, self.lines)
        self.update_count.emit(self.counts[self.cc]) # yes self.cc

    def undo(self):
        if len(self.undos) < 1:
            return
        u = self.undos.pop()
        self.undoredo(u, True)
        self.redos.append(u)

    def redo(self):
        if len(self.redos) < 1:
            return
        u = self.redos.pop()
        self.undoredo(u, False)
        self.undos.append(u)


    def pick(self, point):
        point = self.mapToScene(point).toPoint()
        index = -1
        cat = -1
        i = 0
        for c in range(len(self.cats)-1, -1, -1):
            if not self.video.show_all_cats and c != self.cc:
                next
            for i in range(len(self.cats[c])-1, -1, -1):
                tp = self.cats[c][i]
                if self.video.do_show_point(tp):
                    d = pow(pow(tp.p.x()-point.x(), 2) + pow(tp.p.y()-point.y(), 2), 0.5)
                    if d <= self.video.draw_width + max(1, int(self.video.draw_width/4)):
                        #print("Picked! %d %d" % (c , i))
                        cat = c
                        index = i
                        break
            if cat != -1:
                break
        return(cat, index)

    def mouse_click(self, point, button, modifiers):
        if self.image.isUnderMouse() and not self.video.is_mosaic():
            if self.lines_editor is not None:
                point = self.mapToScene(point).toPoint()

                if self.lines_editor_p1 is None:
                    self.lines_editor_p1 = (point.x(), point.y())
                elif self.lines_editor_p2 is None:
                    self.lines_editor_p2 = (point.x(), point.y())
                    self.lines_editor.set_points(self.lines_editor_p1, self.lines_editor_p2)
                    self.lines_editor = None
            elif button == QtCore.Qt.RightButton or (button == QtCore.Qt.LeftButton and modifiers == QtCore.Qt.ControlModifier): 
                cat, index = self.pick(point)
                if index > -1:
                    self.remove_point(cat, index)
            elif modifiers == QtCore.Qt.ShiftModifier:
                pass
            elif button == QtCore.Qt.LeftButton:
                point = self.mapToScene(point).toPoint()
                tpoint = TQtPoint(self.video.frame_pos, point)
                self.add_one_point(tpoint)

    def mousePressEvent(self, event):
        self.press = event.pos()
        self.press_modifiers = QtWidgets.QApplication.keyboardModifiers()
        # print("mods", self.press_modifiers)
        if event.button() == QtCore.Qt.LeftButton and (self.press_modifiers == QtCore.Qt.ShiftModifier or self.press_modifiers == QtCore.Qt.ControlModifier):
            self.setDragMode(QtWidgets.QGraphicsView.RubberBandDrag)
        super(ImageViewer, self).mousePressEvent(event)
        if self.dragMode() == QtWidgets.QGraphicsView.RubberBandDrag:
            self.viewport().setCursor(QtCore.Qt.CrossCursor)
        else:
            self.viewport().setCursor(QtCore.Qt.ArrowCursor)

    # override
    def mouseMoveEvent(self, event):
        p = event.pos()
        if self.press is not None:
            dist =  ((p.x() - self.press.x())**2 + (p.y() - self.press.y())**2)**0.5
            if dist > 3:
               self.press = None
        super(ImageViewer, self).mouseMoveEvent(event)
        #print("mouse")
        #if self._cross_cursor: 
        #    self._cross_cursor = False
        #    print("set cross cursor")
        #    self.viewport().setCursor(QtCore.Qt.CrossCursor)

    # override
    def mouseReleaseEvent(self, event):
        if self.press is not None:
            self.mouse_click(event.pos(), event.button(), QtWidgets.QApplication.keyboardModifiers())
        else:
            rect = self.rubberBandRect()
            if not rect.isNull() and event.button() == QtCore.Qt.LeftButton:
                rect = self.mapToScene(rect).boundingRect().toRect()
                #print("Get a rectangle:", rect)
                if self.press_modifiers == QtCore.Qt.ShiftModifier:
                    pass # for now
                elif self.press_modifiers == QtCore.Qt.ControlModifier:
                    self.remove_points(rect)
        self.press = None
        #self._next_drag = ""
        super(ImageViewer, self).mouseReleaseEvent(event)
        if self.dragMode() == QtWidgets.QGraphicsView.RubberBandDrag:
            self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)
        # if self._save_drag_mode is not None:
        #     self.setDragMode(self._save_drag_mode)
        #     self._save_drag_mode = None
        self.viewport().setCursor(QtCore.Qt.ArrowCursor)

    # override 
    def enterEvent(self, event):
        super().enterEvent(event)
        self.viewport().setCursor(QtCore.Qt.ArrowCursor)

    # override
    def wheelEvent(self, event):
        if self.has_image():
            if event.angleDelta().y() > 0:
                factor = 1.25
            else:
                factor = 0.8
            self.scale(factor, factor)


class Window(QtWidgets.QWidget):
    def __init__(self, args):
        super(Window, self).__init__()
        self.video = Video(self, args)
        self.viewer = ImageViewer(self, self.video)
        self.viewer.point_added.connect(self.point_added)
        self.viewer.reset_count_file.connect(self.reset_count_file)
        self.viewer.scene_changed.connect(self.scene_changed)

        self.vtimer = QtCore.QTimer()
        self.vtimer.timeout.connect(self.vtime_timout)
        self.vspeed = 0
        self.speed_factor = 1.0
        self.ar_wait = True

        self.detect = args["detect"]

        # default
        self.cat_names = ["main", "back"]
        self.cat_colors = [(0,255,0), (0,0,255)]
        self.current_cat = 0

        # files
        self._count_filename = None
        self._cfile = None

        # lines editor
        self.le = None
        self.allow_le = False

        self.setWindowIcon(QtGui.QIcon(resource_path('data/icons/pointsverts.ico')))
        # main layout
        VBlayout = QtWidgets.QVBoxLayout(self)

        #### top bar
        topHBlayout = QtWidgets.QHBoxLayout()
        topHBlayout.setAlignment(QtCore.Qt.AlignLeft)

        ### menubar
        menubar = QtWidgets.QMenuBar()
        ##
        actionFile = menubar.addMenu("File")
        #
        openAct = QtWidgets.QAction('Open Video/Image', self)
        openAct.setShortcut('Ctrl+O')
        openAct.triggered.connect(self.open_video)
        actionFile.addAction(openAct)
        #
        countAct  = QtWidgets.QAction('Load count template', self)
        countAct.triggered.connect(self.load_count_template)
        actionFile.addAction(countAct)
        #
        exportAct = QtWidgets.QAction('Export to Video/Image', self)
        exportAct.setShortcut('Ctrl+P')
        exportAct.triggered.connect(self.print_video)
        actionFile.addAction(exportAct)
        #
        quitAct = QtWidgets.QAction('Quit', self)
        #quitAct.setShortcut('Ctrl+Q')
        quitAct.triggered.connect(QtWidgets.QApplication.quit)
        actionFile.addAction(quitAct)
        ##
        actionEdit = menubar.addMenu("Edit")
        #
        undoAct = QtWidgets.QAction('Undo', self)
        undoAct.setShortcut('Ctrl+Z')
        undoAct.triggered.connect(self.undo)
        actionEdit.addAction(undoAct)
        #
        redoAct = QtWidgets.QAction('redo', self)
        redoAct.setShortcut('Ctrl+Y')
        redoAct.triggered.connect(self.redo)
        actionEdit.addAction(redoAct)
        #
        addCatAct = QtWidgets.QAction('Add a category', self)
        addCatAct.triggered.connect(self.add_a_category)
        actionEdit.addAction(addCatAct)
        #
        editCatAct = QtWidgets.QAction('Edit the current category', self)
        editCatAct.triggered.connect(self.edit_current_category)
        actionEdit.addAction(editCatAct)
        #
        self.removeCatAct = QtWidgets.QAction('Remove the current category', self)
        self.removeCatAct.triggered.connect(self.remove_current_category)
        actionEdit.addAction(self.removeCatAct)
        self.removeCatAct.setDisabled(True)
        #
        self.linesEditAct = QtWidgets.QAction('Lines Editor', self)
        self.linesEditAct.triggered.connect(self.lines_editor)
        self.linesEditAct.setDisabled(True)
        actionEdit.addAction(self.linesEditAct)
        ##
        actionShow = menubar.addMenu("Show")
        #
        self.showallcatsAct = QtWidgets.QAction('Show all Categories', self)
        self.showallcatsAct.setShortcut('C')
        self.showallcatsAct.setCheckable(True)
        self.showallcatsAct.triggered.connect(self.toggle_show_all_cats)
        actionShow.addAction(self.showallcatsAct)
        #
        self.showallpointsAct = QtWidgets.QAction('Show all Dots', self)
        self.showallpointsAct.setShortcut('D')
        self.showallpointsAct.setCheckable(True)
        self.showallpointsAct.triggered.connect(self.toggle_show_all_points)
        actionShow.addAction(self.showallpointsAct)
        ##
        topHBlayout.addWidget(menubar)

        ### Tool buttons
        catlab = QtWidgets.QLabel(self)
        catlab.setText('Categories:')
        topHBlayout.addWidget(catlab)
        self.cbCategories  = PVComboBox(self)
        self.cbCategories.setFixedWidth(150) # FIXME
        self.update_cb_cat_names()
        self.cbCategories.setCurrentIndex(0)
        self.cbCategories.currentIndexChanged.connect(self.cb_categories_index_changed)
        topHBlayout.addWidget(self.cbCategories)
        #
        self.countInfo = QtWidgets.QLineEdit(self)
        self.countInfo.setFixedWidth(200)
        self.countInfo.setReadOnly(True)
        self.countInfo.setText('0')
        topHBlayout.addWidget(self.countInfo)
        self.viewer.update_count.connect(self.update_count)
        #
        dotssizelab = QtWidgets.QLabel(self)
        dotssizelab.setText('Dots size:')
        topHBlayout.addWidget(dotssizelab)
        self.cbDrawSize  = PVComboBox(self)
        self.cbDrawSize.addItem("1")
        self.cbDrawSize.addItem("2")
        self.cbDrawSize.addItems(["3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "16", "20"])
        self.cbDrawSize.setCurrentIndex(5)
        self.cbDrawSize.currentIndexChanged.connect(self.change_drawing_size)
        topHBlayout.addWidget(self.cbDrawSize)
        self.video.update_drawing_size(6)
        ##
        VBlayout.addLayout(topHBlayout)

        #### viewer
        VBlayout.addWidget(self.viewer)

        #### slider
        self.ignore_slider_value = False
        self.slider = PVSlider(QtCore.Qt.Horizontal)
        self.slider.valueChanged.connect(self.slider_change)
        self.slider.sliderPressed.connect(self.slider_pressed)
        self.slider_max = 999
        self.slider.setMinimum(0)
        self.slider.setMaximum(999)
        # self.slider.sliderMoved.connect(self.slider_moved)
        self.slider.sliderReleased.connect(self.slider_released)
        self.slider_restart_vtimer = False
        VBlayout.addWidget(self.slider)

        #### bottom bar
        HBlayout = QtWidgets.QHBoxLayout()
        HBlayout.setAlignment(QtCore.Qt.AlignCenter)
        #HBlayout.setContentsMargins(100, 0, 100, 0) # left, top, right, bottom
        videoHBlayout = QtWidgets.QHBoxLayout()
        videoHBlayout.setAlignment(QtCore.Qt.AlignRight)
        #videoHBlayout.setContentsMargins(100, 0, 100, 0) # left, top, right, bottom
        videoHBlayout.addStretch()
        ##
        btnBack = QtWidgets.QToolButton(self)
        icon = self.style().standardIcon(QtWidgets.QStyle.SP_MediaSkipBackward)
        btnBack.setIcon(icon)
        btnBack.clicked.connect(self.back)
        videoHBlayout.addWidget(btnBack)
        #HBlayout.addWidget(btnBack)
        ##
        self.btnPlay = QtWidgets.QToolButton(self)
        self.icon_play = self.style().standardIcon(QtWidgets.QStyle.SP_MediaPlay)
        self.icon_pause = self.style().standardIcon(QtWidgets.QStyle.SP_MediaPause)
        self.btnPlay.setIcon(self.icon_play)
        self.btnPlay.clicked.connect(self.play)
        videoHBlayout.addWidget(self.btnPlay)
        #HBlayout.addWidget(self.btnPlay)
        ##
        self.labelSpeed= QtWidgets.QLabel(self)
        self.labelSpeed.setText('   Speed: x1.0')
        videoHBlayout.addWidget(self.labelSpeed)
        #HBlayout.addWidget(labelSpeed)
        btnSlower = QtWidgets.QToolButton(self)
        icon = self.style().standardIcon(QtWidgets.QStyle.SP_MediaSeekBackward)
        btnSlower.setIcon(icon)
        btnSlower.clicked.connect(self.slower)
        videoHBlayout.addWidget(btnSlower)
        #HBlayout.addWidget(btnSlower)
        btnFaster   = QtWidgets.QToolButton(self)
        icon = self.style().standardIcon(QtWidgets.QStyle.SP_MediaSeekForward)
        btnFaster.setIcon(icon)
        btnFaster.clicked.connect(self.faster)
        videoHBlayout.addWidget(btnFaster)
        #videoHBlayout.addStretch()
        #HBlayout.addWidget(btnFaster)
        ##
        infoHBlayout= QtWidgets.QHBoxLayout()
        infoHBlayout.setAlignment(QtCore.Qt.AlignRight)
        pixInfoLabel = QtWidgets.QLabel(self)
        pixInfoLabel.setText('pix info:')
        infoHBlayout.addStretch()
        infoHBlayout.addWidget(pixInfoLabel)
        self.pixInfo = QtWidgets.QLineEdit(self)
        self.pixInfo.setReadOnly(True)
        self.pixInfo.setFixedWidth(300)
        infoHBlayout.addWidget(self.pixInfo)
        #HBlayout.addWidget(self.pixInfo)
        ##
        HBlayout.addLayout(videoHBlayout)
        HBlayout.addLayout(infoHBlayout)
        VBlayout.addLayout(HBlayout)

        play_shortcut = QtWidgets.QShortcut(QtGui.QKeySequence("Space"), self)
        play_shortcut.activated.connect(self.play)
        fs_shortcut = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_F11), self)
        fs_shortcut.activated.connect(self.toggle_fullscreen)
        efs_shortcut = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Escape), self)
        efs_shortcut.activated.connect(self.escape_fullscreen)

        allow_le_shortcut = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_E), self)
        allow_le_shortcut.activated.connect(self.allow_lines_editor)

        detect_shortcut = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_L), self)
        detect_shortcut.activated.connect(self.enable_detect)
        detect_toggle_shortcut = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_L), self)
        detect_toggle_shortcut.activated.connect(self.toggle_detect)
        detect_image_shortcut = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_I), self)
        detect_image_shortcut.activated.connect(self.next_detect_image)
        detect_contour_shortcut = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_O), self)
        detect_contour_shortcut.activated.connect(self.next_detect_contour)

    def post_init(self, filename, opt_count_file = None, opt_count_in = None):
        #if not os.path.isfile(filename):
        #    print(filename, "does not exist!")
        #    return
        #if cv2.haveImageReader(filename):
        #    print(filename, "is an image")
        bn =  os.path.basename(filename)
        self.setWindowTitle("pointsverts: "+bn)
        ret = self.video.load_video(filename)
        if not ret:
            self.show_okdialog("fail to load "+bn, "fail to load")
            self.viewer.set_image(None)
            return(False)
        db = os.path.splitext(filename)[0]
        dbcin = dbcout= db + ".count"
        if opt_count_file is not None:
            dbcout = opt_count_file
            if opt_count_in is None:
                dbcin = opt_count_file
        if opt_count_in is not None:
            if os.path.isfile(opt_count_in):
                dbcin = opt_count_in
            else:
                self.show_okdialog("fail to open: " + opt_count_in+ "\n\n we will exit", "fail to open")
                exit()
        if os.path.isfile(dbcin):
            print("load count file: " + dbcin)
            self._cfile = open(dbcin, 'r')
            dlines = []
            cats = []
            cats.append([])  # to support before cat count file
            first_cat = True # to support before cat count file
            version = 0
            for line in self._cfile:
                p = line.strip().split(",")
                if p[0] == "Version":
                    version = p[1]
                elif p[0] == "Cat": # "Cat",c,name,b,g,r
                    if first_cat:
                        first_cat = False
                        self.cat_names.clear()
                        self.cat_colors.clear()
                    index = int(p[1])
                    if index > 0:
                       cats.append([])
                    self.cat_names.append(p[2])
                    self.cat_colors.append( (int(p[3]), int(p[4]), int(p[5])) )
                elif p[0] == "Line":
                    dlines.append(Line((int(p[1]), int(p[2])), (int(p[3]), int(p[4])), (int(p[5]), int(p[6]), int(p[7]))))
                elif len(p) == 3: # befor version 0.2
                    cats[0].append(TQtPoint(int(p[0]), QtCore.QPoint(int(p[1]), int(p[2]))))
                elif len(p) == 4:
                    index = int(p[0])
                    if index < len(self.cat_names):
                        cats[index].append(TQtPoint(int(p[1]), QtCore.QPoint(int(p[2]), int(p[3]))))
                elif len(p) == 2: # pointsrouges compatibility
                    cats[0].append(TQtPoint(0, QtCore.QPoint(int(p[0]), int(p[1]))))

            #print("loaded %d categories" % len(cats))
            c = 0
            for c in range(len(self.cat_names)):
                pl = 0
                if c < len(cats):
                    pl = len(cats[c])
                print(self.cat_names[c], ": points loaded %d" % pl)
                self.viewer.add_category(c)
                if c < len(cats):
                    self.viewer.reset_points_data(c, cats[c])
                else:
                    self.viewer.reset_points_data(c, []) # not needed I guess
            self.viewer.set_lines(dlines)
            self.update_cb_cat_names()
            self._cfile.close()
            self._cfile = open(dbcout, 'a')
            self._count_filename = dbcout
            if version != "0.2" or dbcin != dbcout:
                print("reset count file for version reason")
                self.reset_count_file(self.viewer.cats, self.viewer.lines)
        else:
            print("count file does not exist, create it: " + dbcout)
            c = 0
            for c in range(len(self.cat_names)):
                self.viewer.add_category(c)
                self.viewer.reset_points_data(c, []) # not needed I guess
            self.update_cb_cat_names()
            try:
                self._cfile = open(dbcout, 'w')
            except:
                self.show_okdialog("fail to write on: "+ dbcout + "\n\n use the -c option?\n\n we will exit!", "fail to write")
                exit()
            self.write_header()
            self._cfile.flush()
        self._count_filename = dbcout
        self.video.set_cat_colors(self.cat_colors)
        self.show_next_frame()
        self.vspeed = int(1000/self.video.fps)
        self.speed_factor = 1
        self.update_speed()
        self.viewer.fit_in_view()
        if self.allow_le:
            self.linesEditAct.setDisabled(False)
        return(True)

    def clean_up(self):
        self.viewer.clean_data()
        if self._cfile is not None:
            self._cfile.close()
        self.cat_names = ["main", "back"]
        self.cat_colors = [(0,255,0), (0,0,255)]
        self.current_cat = 0
        self.video.show_all_cats = False
        self.showallcatsAct.setChecked(False)
        self.video.show_all_points = False
        self.showallpointsAct.setChecked(False)
        if self.le is not None:
            self.le.close()
        self.linesEditAct.setDisabled(True)

    def open_video(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"Open", "","All Files (*);;MP4 (*.mp4);;AVI(*.avi);;JPEG (*.jpeg);;JPG (*.jpg);;PNG (*.png)", options=options)
        if fileName:
            print("attept to load: " + fileName)
            self.clean_up()
            self.post_init(fileName)

    def load_count_template(self):
        if not self.viewer.has_image():
            return
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        file_name, _ = QFileDialog.getOpenFileName(self,"Open", "","count (*.count);; All Files (*)", options=options)
        if file_name:
            print("attept to load template count file: " + file_name)
            # s = sum(self.viewer.counts)
            file = open(file_name, 'r')
            dlines = []
            for line in file:
                p = line.strip().split(",")
                if p[0] == "Version":
                    version = p[1]
                elif p[0] == "Cat": # "Cat",c,name,b,g,r
                    index = int(p[1])
                    if index > len(self.cat_names):
                        print("error in loading: " + file_name + ", category index error!")
                    elif index >= len(self.cat_names):
                        self.cat_names.append(p[2])
                        self.cat_colors.append( (int(p[3]), int(p[4]), int(p[5])) )
                        self.viewer.add_category(index)
                    else:
                        self.cat_names[index] = p[2]
                        self.cat_colors[index] = (int(p[3]), int(p[4]), int(p[5]))
                        #self.cbCategories.setItemIcon(index, ColorIcon(12, qcol))
                        #self.cbCategories.setItemText(index, " "+ name +":")
                elif p[0] == "Line":
                    dlines.append(Line((int(p[1]), int(p[2])), (int(p[3]), int(p[4])), (int(p[5]), int(p[6]), int(p[7]))))
            #
            self.viewer.set_lines(dlines)
            self.update_cb_cat_names()
            self.reset_count_file(self.viewer.cats, self.viewer.lines)
            if not self.vtimer.isActive():
                self.redraw_current_frame()

    def show_next_frame(self, from_slider=False):
        pix = self.video.get_next_frame(self.viewer.cc, self.viewer.cats, self.viewer.lines)
        if pix is not None:
            self.viewer.set_image(pix)
            v = max(0, int((self.slider_max+1)*self.video.frame_pos/self.video.nbr_frames)-1)
            # print("snf slider ", v)
            if not from_slider:
                self.ignore_slider_value = True
                self.slider.setValue(v)
                self.ignore_slider_value = False
        else:
            self.vtimer.stop()
            #self.slider.setValue(self.slider_max)
            self.btnPlay.setIcon(self.icon_play)

    def show_prev_frame(self,f=1):
        self.video.set_frame_pos(self.video.frame_pos-f)
        self.show_next_frame()

    def redraw_current_frame(self):
        pix = self.video.get_current_frame(self.viewer.cc, self.viewer.cats, self.viewer.lines)
        if pix is not None:
            self.viewer.set_image(pix)
        else:
            self.vtimer.stop()
            self.btnPlay.setIcon(self.icon_play)

    def toggle_show_all_points(self):
        self.video.toggle_show_all_points()
        self.showallpointsAct.setChecked(self.video.show_all_points)
        self.viewer.redraw_points()

    def toggle_show_all_cats(self):
        self.video.toggle_show_all_cats()
        self.showallcatsAct.setChecked(self.video.show_all_cats)
        self.viewer.redraw_points()

    def undo(self):
        self.viewer.undo()

    def redo(self):
        self.viewer.redo()

    def point_added(self, c, tp):
       self.pixInfo.setText('c: %d, p: (%d, %d), f: %d' % (c, tp.p.x(), tp.p.y(), tp.f))
       if self._cfile is not None:
          self._cfile.write('%d,%d,%d,%d\n' % (c, tp.f, tp.p.x(), tp.p.y()))
          self._cfile.flush()

    def write_header(self):
        self._cfile.write('Version,0.2,DO NOT EDIT\n')
        c = 0
        for c in range(len(self.cat_names)):
            col = self.cat_colors[c]
            self._cfile.write('Cat,%d,%s,%d,%d,%d\n' % (c, self.cat_names[c],col[0],col[1],col[2]))
            c += 1

    def reset_count_file(self, cats, lines):
        if self._cfile is None:
            return
        self._cfile.close()
        #print(self._count_filename)
        self._cfile = open(self._count_filename, 'w')
        self.write_header()
        for l in lines:
            self._cfile.write('Line,%d,%d,%d,%d,%d,%d,%d\n' % (l.p1[0], l.p1[1], l.p2[0], l.p2[1], l.color[0], l.color[1], l.color[2]))
        c = 0
        while c < len(cats):
            for pt in cats[c]:
                self._cfile.write('%d,%d,%d,%d\n' % (c, pt.f, pt.p.x(), pt.p.y()))
            c += 1
        self._cfile.flush()

    def write_template(self, fname, lines):
        file = open(fname, 'w')
        file.write('Version,0.2,DO NOT EDIT\n')
        c = 0
        for c in range(len(self.cat_names)):
            col = self.cat_colors[c]
            file.write('Cat,%d,%s,%d,%d,%d\n' % (c, self.cat_names[c],col[0],col[1],col[2]))
            c += 1
        for l in lines:
            file.write('Line,%d,%d,%d,%d,%d,%d,%d\n' % (l.p1[0], l.p1[1], l.p2[0], l.p2[1], l.color[0], l.color[1], l.color[2]))
        file.close()

    def show_okdialog(self, text, title, buttons = QtWidgets.QMessageBox.Ok):
        msg = QtWidgets.QMessageBox(self)
        msg.setText(text)
        msg.setWindowTitle(title)
        msg.setStandardButtons(buttons)
        retval = msg.exec_()
        return retval

    # issue it calls itsef ...
    def cb_categories_index_changed(self, v):
        # print("category", v)
        l = len(self.cat_names)
        if v < 0:
            return
        if v < l:
            if self.current_cat != v:
                #print("form %s to %s" % (self.cat_names[self.current_cat], self.cat_names[v]))
                self.current_cat = v
                self.viewer.change_category(v)
                if v >= 2:
                    self.removeCatAct.setDisabled(False)
                else:
                    self.removeCatAct.setDisabled(True)

    def update_cb_cat_names(self):
        self.cbCategories.clear()
        for c in range(len(self.cat_names)):
            n = self.cat_names[c]
            col = self.cat_colors[c]
            qcol = QtGui.QColor(col[2], col[1], col[0])
            self.cbCategories.addItem(ColorIcon(12, qcol), " "+ n +":")
        if self.current_cat > 1:
            self.removeCatAct.setDisabled(False)

    def add_a_category(self):
        ac = CategoryDialog(self, "Add")
        if ac.exec():
            qcol, name = ac.get_result()
            self.cat_names.append(name)
            col = (qcol.blue(), qcol.green(), qcol.red())
            self.cat_colors.append(col)
            l = len(self.cat_names)
            self.viewer.add_category(l-1)
            self.reset_count_file(self.viewer.cats, self.viewer.lines)
            self.update_cb_cat_names()
            self.cbCategories.setCurrentIndex(l-1)
            self.viewer.change_category(l-1)
            self.reset_count_file(self.viewer.cats, self.viewer.lines)

    def edit_current_category(self):
        col = self.cat_colors[self.current_cat]
        qcol = QtGui.QColor(col[2], col[1], col[0])
        ac = CategoryDialog(self, "Edit", qcol, self.cat_names[self.current_cat])
        if ac.exec():
            qcol, name = ac.get_result()
            #print("result", col, name)
            self.cat_names[self.current_cat] = name
            col = (qcol.blue(), qcol.green(), qcol.red())
            self.cat_colors[self.current_cat] = col
            self.cbCategories.setItemIcon(self.current_cat, ColorIcon(12, qcol))
            self.cbCategories.setItemText(self.current_cat, " "+ name +":")
            self.reset_count_file(self.viewer.cats, self.viewer.lines)
            self.viewer.redraw_points()

    def remove_current_category(self):
        if self.current_cat <= 1: # should be false anyway
                return
        conf = self.show_okdialog("Are you sure you want to remove\n the "+self.cat_names[self.current_cat]+" category?", "Are you sure?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        if conf == QtWidgets.QMessageBox.Yes:
            del self.cat_names[self.current_cat]
            del self.cat_colors[self.current_cat]
            self.viewer.remove_category(self.current_cat)
            self.current_cat = self.current_cat - 1
            self.update_cb_cat_names() # auto switch to cat main and redraw
            self.reset_count_file(self.viewer.cats, self.viewer.lines)

    def lines_editor(self):
        self.le = LinesEditor(self, self.viewer.lines)
        self.le.show()

    def allow_lines_editor(self):
        self.allow_le = True
        if self.viewer.has_image():
            self.linesEditAct.setDisabled(False)

    def update_count(self, count):
        self.countInfo.setText('%d' % count)

    def vtime_timout(self):
        self.show_next_frame()

    def back(self):
        self.video.set_frame_pos(0)
        self.slider.setValue(0)

    def play(self):
        if self.vtimer.isActive():
            self.vtimer.stop()
            self.btnPlay.setIcon(self.icon_play)
        else:
            self.vtimer.start(self.vspeed)
            self.btnPlay.setIcon(self.icon_pause)

    def update_speed(self):
        self.vspeed = int(1000/(self.video.fps*self.speed_factor))
        s = "Speed: x" + '{:.1f}'.format(self.speed_factor)
        self.labelSpeed.setText(s)
        if self.vtimer.isActive():
            self.vtimer.stop()
            self.vtimer.start(self.vspeed)

    def faster(self):
        self.speed_factor += 0.2
        self.update_speed()

    def slower(self):
        self.speed_factor -= 0.2
        if self.speed_factor < 0.2:
            self.speed_factor = 0.2
        self.update_speed()

    def toggle_fullscreen(self):
        if not self.isFullScreen():
            self.showFullScreen()
        else:
            self.showNormal()

    def escape_fullscreen(self):
         if self.isFullScreen():
            self.showNormal()

    def enable_detect(self):
        self.video.enable_detect()
        self.detect = True

    def toggle_detect(self):
        if not self.detect:
            return
        self.video.toggle_detect()
        if not self.vtimer.isActive():
            self.redraw_current_frame()

    def next_detect_image(self):
        if not self.detect or not self.video.detect:
            return
        self.video.next_shows_image()
        if not self.vtimer.isActive():
            self.redraw_current_frame()

    def next_detect_contour(self):
        if not self.detect or not self.video.detect:
            return
        self.video.next_draw_contour()
        if not self.vtimer.isActive():
            self.redraw_current_frame()

    def slider_pressed(self):
        #print("slider pressed")
        if self.vtimer.isActive():
            self.vtimer.stop()
            self.slider_restart_vtimer = True

    # def slider_moved(self):
    #     print("slider moved")

    def slider_change(self, v):
        #print("slider value", v, self.ignore_slider_value)
        if self.ignore_slider_value:
            return
        v = self.slider.value()
        f = int((v/(self.slider_max+1))*self.video.nbr_frames)
        self.video.set_frame_pos(f)
        if f > 0:
            f -= 1
        #print("slider value", v, f)
        self.video.set_frame_pos(f)
        self.show_next_frame(True)

    def slider_released(self):
        #print("slider released")
        if self.slider_restart_vtimer:
            self.vtimer.start(self.vspeed)
            self.slider_restart_vtimer = False

    def change_drawing_size(self,i):
        self.video.update_drawing_size(i+1)
        self.viewer.redraw_points()

    # override
    def resizeEvent(self, event):
        self.viewer.fit_in_view()

    # event from the viewer
    def scene_changed(self):
        # print("scene_changed")
        self.ar_wait = False

    # key event helper
    def next(self, event):
        self.show_next_frame()
        self.show_next_frame()

    def prev(self, event):
        if not event.isAutoRepeat():
            self.ar_wait = True
            self.show_prev_frame(2)
        elif not self.ar_wait:
            self.ar_wait = True
            self.show_prev_frame(4)

    def prev_long(self, event):
        if not event.isAutoRepeat():
            self.show_prev_frame(int(2*self.video.fps*self.speed_factor))

    # override
    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Z:
            if QtWidgets.QApplication.keyboardModifiers() != QtCore.Qt.ControlModifier:
                self.next(event)
        elif event.key() == QtCore.Qt.Key_A:
            self.prev(event)
        elif event.key() == QtCore.Qt.Key_Q:
            self.prev_long(event)

    def print_video(self):
        if not self.viewer.has_image():
            return
        if self.vtimer.isActive():
            self.vtimer.stop()
            self.btnPlay.setIcon(self.icon_play)
        db = os.path.splitext(self._count_filename)[0]
        ext = ".mp4"
        if self.video.nbr_frames == 1:
            ext = ".jpg"
        s = sum(self.viewer.counts)
        dbv = db + "-print-" + str(s) + ext
        name , _ = QFileDialog.getSaveFileName(self, 'Save File', dbv)
        if name:
            if self.video.nbr_frames == 1:
                self.video.show_all_cats = True
                self.video.show_all_points = True
                pix = self.video.get_current_frame(self.viewer.cc, self.viewer.cats, self.viewer.lines)
                pix.save(name, "JPG")
                return
            progress = QtWidgets.QProgressDialog("exporting to\n"+name, "Cancel", 0, int(self.video.nbr_frames + 4*self.video.fps), self)
            progress.setWindowTitle("exporting")
            progress.setWindowModality(QtCore.Qt.WindowModal)
            progress.show()
            QtWidgets.QApplication.processEvents()
            print("exporting to:", name, self.video.width, self.video.height, self.video.fps)
            #fourcc = cv2.VideoWriter_fourcc(*"mp4v")
            fourcc = cv2.VideoWriter_fourcc('H','2','6','4');
            dim = (int(self.video.width), int(self.video.height))
            writer = cv2.VideoWriter(name, fourcc, int(self.video.fps), dim, True)
            self.video.set_frame_pos(0)
            self.video.show_all_cats = True
            # sorter = lambda x: x[1].f
            # sorted_cats = sorted(self.viewer.cats, key=sorter)
            # print(sorted_cats)
            text_h = 0
            text_w = 0
            for c in range(len(self.viewer.cats)):
                t = self.cat_names[c] + ": " +  str(len(self.viewer.cats[c]))
                size = cv2.getTextSize(t, cv2.FONT_HERSHEY_SIMPLEX, 1, 2)
                #print("w:", w, "h:", h)
                text_w = max(text_w, size[0][0])
                text_h = max(text_h, size[0][1])
            #print("max text dim:", text_w, text_h)
            textpos_init = (int(1.5*text_h), int(text_h + text_h))
            textinc = int(text_h + 0.8*text_h)
            rect_h = int(text_h + len(self.viewer.cats)*(0.8*text_h + 0.8*text_h)+ 0.8*text_h)
            rect_w = int (text_w + 4*text_h)
            s = str("")
            for fc in range(int(self.video.nbr_frames)):
                img = self.video.get_next_frame(0, self.viewer.cats, self.viewer.lines, ret_frame=True)
                img = cv2.rectangle(img, (0, 0), (rect_w, rect_h), (0, 0, 0), -1)
                textpos = textpos_init
                for c in range(len(self.viewer.cats)):
                    num = len([p for p in self.viewer.cats[c] if p.f <= fc])
                    s = self.cat_names[c] + ": " +  str(num)
                    # print(s, textpos)
                    img = cv2.putText(img, s, textpos, cv2.FONT_HERSHEY_SIMPLEX,  1, self.cat_colors[c], 2, cv2.LINE_AA)
                    textpos = (textpos[0], textpos[1] + textinc)
                writer.write(img)
                progress.setValue(fc)
                QtWidgets.QApplication.processEvents()
                if progress.wasCanceled():
                    break

            progress.setLabelText("add all dots on final frame\n"+name)
            pfc = int(self.video.nbr_frames-1)
            if progress.wasCanceled():
                progress.reset()
                progress.setValue(pfc)
                progress.show()
                QtWidgets.QApplication.processEvents()

            self.video.set_frame_pos(self.video.nbr_frames-2)
            save_show_all_cats = self.video.show_all_cats
            self.video.show_all_cats = True
            save_show_all_points = self.video.show_all_points
            self.video.show_all_points = True
            img = self.video.get_next_frame(0, self.viewer.cats, self.viewer.lines, ret_frame=True)
            img = cv2.rectangle(img, (0, 0), (rect_w, rect_h), (0, 0, 0), -1)
            textpos = textpos_init
            for c in range(len(self.viewer.cats)):
                num = len(self.viewer.cats[c])
                s = self.cat_names[c] + ": " +  str(num)
                img = cv2.putText(img, s, textpos, cv2.FONT_HERSHEY_SIMPLEX,  1, self.cat_colors[c], 2, cv2.LINE_AA)
                textpos = (textpos[0], textpos[1] + textinc)
            for fc in range(int(4*self.video.fps)):
                writer.write(img)
                pfc +=1
                progress.setValue(pfc)
                QtWidgets.QApplication.processEvents()
                if progress.wasCanceled():
                    break
            writer.release()
            progress.setValue(int(self.video.nbr_frames + 4*self.video.fps))
            self.video.show_all_points = save_show_all_points
            self.video.show_all_cats = save_show_all_cats
            print("done")

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    parse = argparse.ArgumentParser()
    # parse.add_argument("-i", "--input", required=True, type=str, help="path to video file")
    parse.add_argument("-v", "--version", help="print version and exit", action="store_true")
    parse.add_argument("-i", "--input", type=str, help="path to a video/image file")
    parse.add_argument("-ci", "--countin", type=str, help="load an other file that the default count file")
    parse.add_argument("-c", "--countfile", type=str, help="use an other file that the default count file")
    parse.add_argument("-p", "--persistence", type=int, default=25, help="number of frames an added green point stays on the video")
    parse.add_argument("-d", "--detect", help="enable elementary detect mode", action="store_true")
    parse.add_argument('file', metavar='file', type=str, nargs='?', help="path to a video/image file (similart to the -i/--input option)")
    args = vars(parse.parse_args())
    # print("detect", args["detect"])
    if args["version"]:
        print("pointsverts version", VERSION)
        sys.exit(0)
    window = Window(args)
    screen = app.primaryScreen()
    sg = screen.size()
    #sg = QtGui.QDesktopWidget().screenGeometry()
    window.resize(int(sg.width()/2), int(sg.height()/2))
    window.setWindowTitle('pointsverts')
    window.show()
    #
    basedir = None
    if args["input"] is None:
        args["input"] = args["file"]
    if args["input"] is not None:
        args["input"] = os.path.abspath(args["input"])
        if os.path.isfile(args["input"]):
            basedir = os.path.dirname(args["input"])
        elif os.path.exists(args["input"]):
            basedir = args["input"]
    if args["countfile"] is not None:
        args["countfile"] = os.path.abspath(args["countfile"])
        if args["countin"] is not None:
            args["countin"] = os.path.abspath(args["countin"])
        else:
            args["countin"] = args["countfile"]
    elif args["countin"] is not None:
        args["countin"] = os.path.abspath(args["countin"])
    ret = True
    if args["input"] is not None:
        ret = window.post_init(args["input"], args["countfile"], args["countin"])
    if (not ret or args["input"] is None) and not os.path.isfile("pointsverts.py"):
        # for packages
        basedir = os.path.expanduser("~")
    if basedir is not None:
        os.chdir(basedir)
    sys.exit(app.exec_())
